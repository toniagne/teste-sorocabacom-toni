@extends('adminlte::page')

@section('title', 'JOGO - PERSONAGENS')

@section('content_header')
    <h1>Personagens</h1>
    <ol class="breadcrumb">
        <li><a href=""> Início </a></li>
        <li><a href=""> Personagens </a></li>
        <li><a href=""> <span>Cadastrar novo Personagem</span> </a></li>
    </ol>
@stop

@section('content')
     <div class="box">
         <div class="box-header">
            NOVO PERSONAGEM
         </div>
         <div class="box-body">
             
         <div class="box">
         @if(isset($character))
                    {!! Form::model($character, ['route' => ['character-update', $character->character_id], 'method' =>
                    'put', 'files' => true]) !!}
         @else
                    {!! Form::open(['method' => 'post', 'autocomplete' => 'off', 'route' => ['character-create'],
                    'files' => true]) !!}
         @endif
       
               <BR>
          
                <div class="form-group">
                        {!!Form::label('description', 'Descrição')!!} 
                        {!!Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'DIGITE UM TEXTO ...']) !!}
                </div>
               

                <div class="img-push">
                @if(isset($character))
                <img src="{{ img_src($character->img)}}" width="100%">
                @endif
                    <label>Imagem</label>
                    <input type="file" name="img" class="form-control input-sm" placeholder="Press enter to post comment">
                </div>
                <BR>
                <button type="submit" class="btn btn-block btn-primary">CADASTRAR</button>
              </form>




     </div>
@stop