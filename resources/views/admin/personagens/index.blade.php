@extends('adminlte::page')

@section('title', 'PERSONAGENS')

@section('content_header')
    <h1>Personagens</h1>
    <ol class="breadcrumb">
        <li><a href=""> Início </a></li>
        <li><a href=""> <span>Personagens</span> </a></li>
    </ol>
@stop

@section('content')
     <div class="box">
         <div class="box-header">
         <a href="{!!route('character-new')!!}" class="btn btn-block btn-info">Adicionar Personagem</a>
         </div>
         <div class="box-body">
             
         <div class="box">
           
         @if (session('alert'))
         <div class="alert alert-{!!session('alert.code')!!} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> {!!session('alert.code')!!}!</h4>
                {!! session('alert.text') !!}
         </div>
         @endif


              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Descrição</th>
                  <th>Imagem</th>
                  <th style="width: 140px">#</th>
                </tr>
                @foreach($personagens as $personagem)
                <tr>
                  <td>{{ $personagem->character_id }}</td>
                  <td>{{ $personagem->description }}</td>
                  <td>
                  <img src="{{ img_src($personagem->img) }}" width="80%">
                  </td>
                  <td>
                  <a href="{!!route('character-edit', [$personagem->character_id])!!}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                  <a href="{!!route('character-destroy', [$personagem->character_id])!!}" class="btn btn-default"><i class="fa  fa-eraser"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody></table>
          </div>
        </div> 
     </div>
@stop