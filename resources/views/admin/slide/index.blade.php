@extends('adminlte::page')

@section('title', 'SLIDES')

@section('content_header')
    <h1>Slides</h1>
    <ol class="breadcrumb">
        <li><a href=""> Início </a></li>
        <li><a href=""> <span>Slides</span> </a></li>
    </ol>
@stop

@section('content')
     <div class="box">
         <div class="box-header">
             EDIÇÃO DO SLIDE
         </div>
         <div class="box-body">
         @if (session('alert'))
         <div class="alert alert-{!!session('alert.code')!!} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> {!!session('alert.code')!!}!</h4>
                {!! session('alert.text') !!}
         </div>
         @endif
         
                {!! Form::model(null, ['route' => ['slide-update', 1], 'method' =>
                'post', 'files' => true]) !!}
            <div class="col-md-6"> 
                <div class="box box-widget">
                  <div class="box-header with-border">
                    <div class="user-block"> 
                      <span class="username"><a href="#">IMAGEM BACKGROUND</a></span>
                      <span class="description">Última alteração : {{ $slide->created_at }}</span>
                    </div> 
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <img class="img-responsive pad" src="{{ img_src($slide->img_background) }}" alt="Photo">
                  </div>
                  <!-- /.box-body -->
                 
                  <div class="box-footer">
                    
                        <h5>Selecione outra imagem </h5>
                    <div class="img-push">
                        <input type="file" name="img_background" class="form-control input-sm" placeholder="Press enter to post comment">
                      </div>
                     
                  </div>
                  <!-- /.box-footer -->
                </div>
            </div>
            <div class="col-md-6"> 
                    <div class="box box-widget">
                      <div class="box-header with-border">
                        <div class="user-block"> 
                          <span class="username"><a href="#">IMAGEM PRINCIPAL</a></span>
                          <span class="description">Última alteração : {{ $slide->created_at }}</span>
                        </div> 
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <img class="img-responsive pad" src="{{ img_src($slide->img_principal) }}" alt="Photo">
                      </div>
                      <!-- /.box-body -->
                     
                      <div class="box-footer">
                         
                            <h5>Selecione outra imagem </h5>
                        <div class="img-push">
                            <input type="file" name="img_principal" class="form-control input-sm" placeholder="Press enter to post comment">
                          </div>
                         
                      </div>
                      <!-- /.box-footer -->
                    </div>
            </div>
            <button type="submit" class="btn btn-block btn-primary">ALTERAR</button>
         </form>
        </div> 
     </div>
@stop