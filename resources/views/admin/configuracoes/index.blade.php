@extends('adminlte::page')

@section('title', 'CONFIGURAÇÕES DO SITE')

@section('content_header')
    <h1>Configurações</h1>
    <ol class="breadcrumb">
        <li><a href=""> Início </a></li>
        <li><a href=""> <span>Configurações</span> </a></li>
    </ol>
@stop

@section('content')
     <div class="box">
         <div class="box-header">
             CONFIGURAÇÕES DO SITE
         </div>
         <div class="box-body">
         @if (session('alert'))
         <div class="alert alert-{!!session('alert.code')!!} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> {!!session('alert.code')!!}!</h4>
                {!! session('alert.text') !!}
         </div>
         @endif
         
                {!! Form::model($settings, ['route' => ['settings-update', 1], 'method' =>
                'post']) !!}
           
                <div class="form-group">
                        {!!Form::label('name', 'Nome do Site')!!} 
                        {!!Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'NOME DO SITE ...']) !!}
                </div>

                <div class="form-group">
                        {!!Form::label('presentation', 'Texto de Apresentação')!!} 
                        {!!Form::textarea('presentation', null, ['class' => 'form-control', 'placeholder'=>'TEXTO DE APRESENTAÇÃO ...']) !!}
                </div>

                <div class="form-group">
                        {!!Form::label('description', 'Texto de descrição')!!} 
                        {!!Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'DIGITE UM TEXTO ...']) !!}
                </div>

            <button type="submit" class="btn btn-block btn-primary">ALTERAR</button>
         </form>
        </div> 
     </div>
@stop