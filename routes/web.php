<?php

Route::middleware(['auth'])->namespace('Admin')->group(function () {
    //DESHBOARD
    Route::get('admin', 'AdminController@index')->name('admin');

    // SLIDES
    Route::get('admin/slide', 'SlideController@index')->name('slide');
    Route::post('admin/slideupdate/{id}', 'SlideController@update')->name('slide-update');

    //CONFIGURAÇÕES     
    Route::get('admin/configuracoes', 'ConfiguracoesController@index')->name('configuracoes');
    Route::post('admin/configuracoes/{id}', 'ConfiguracoesController@update')->name('settings-update');

    //  PERSONAGENS
    Route::get('admin/personagens', 'PersonagensController@index')->name('personagens');
    Route::get('admin/personagens/criar', 'PersonagensController@create')->name('character-new');
    Route::get('admin/personagens/editar/{id}', 'PersonagensController@show')->name('character-edit');
    Route::put('admin/personagens/editar/{id}', 'PersonagensController@update')->name('character-update');
    Route::get('admin/personagens/deletar/{id}', 'PersonagensController@destroy')->name('character-destroy');
    Route::post('admin/personagens/novo', 'PersonagensController@store')->name('character-create');

});

 
Route::get('/', 'Site\SiteController@index')->name('home');



Auth::routes();

 
