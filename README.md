# TESTE SOROCABACOM

Projeto para teste prático empresa sorocabacom

# DETALHES DO PROJETO

Projeto desenvolvido para um simples teste prático de produção de conteúdo dinâmico utilizando uma landingpage + cms para adminsitração de conteúdo.

## MÉTODOS E TECNOLOGIAS UTILIZADAS

- [Linguagem PHP](https://www.php.net/)
- [Banco de dados MYSql](https://www.mysql.com/)
- [Framework Laravel 7.1](https://laravel.com/)
- [Tema ADMIN - AdminLTE](https://adminlte.io)

### INSTALAÇÃO
Clonagem do diretório:
```
git clone https://toniagne@bitbucket.org/toniagne/teste-sorocabacom-toni.git
```

Baixe as dependências do projeto via composer. 
```
composer update
```
Configure o autoload
```
composer dump-autoload
```
Baixar as dependências (na raiz do repositório):
```
Atualizar a estrutura do banco de dados  (na raiz do repositório):
```
php artisan migrate
```
Popular o banco de dados com dados para criar usuário de exemplo (login -> exemple@dev.com.br pass-> 123123 ) :
```
php artisan migrate --seed
```

# DESENVOLVEDOR

- Toni Reniê Schott Agne