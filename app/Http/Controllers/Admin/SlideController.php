<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Models\Slide;

class SlideController extends Controller
{
   public function index(){
       $slide = Slide::orderBy("slide_id", "DESC");
       return view('admin.slide.index', array(
        "slide" => $slide->first()
    ));
   }

   public function update(Request $request, $id){
    $data = Input::all(); 
    
        if($request->hasFile('img_background')) {
            $file = $request->file('img_background');
            $input['imagename'] = md5('background'.time()).'.'.$file->getClientOriginalExtension();
            $name_img = md5('background'.time()).'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('img/');
            $file->move($destinationPath, $input['imagename']);
            $data['img_background'] =$name_img;
        }     
        
        if($request->hasFile('img_principal')) {
            $file2 = $request->file('img_principal');
            $input2['imagename2'] = md5('principal'.time()).'.'.$file2->getClientOriginalExtension();
            $name_img2 = md5('principal'.time()).'.'.$file2->getClientOriginalExtension();
            $destinationPath2 = public_path('img/');
            $file2->move($destinationPath2, $input2['imagename2']);
            $data['img_principal'] =$name_img2;
        }       

        Slide::find($id)->update($data);
        $request->session()->flash('alert', array('code'=> 'success', 'text'  => 'Slides Alterados com sucesso'));
        return redirect(route('slide'));               
   }
}
