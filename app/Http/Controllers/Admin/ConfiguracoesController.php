<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Settings;

class ConfiguracoesController extends Controller
{
    public function index(Request $request)
    {
        $settings = Settings::orderBy("settings_id", "DESC");    
        return view('admin.configuracoes.index', array(
           "settings" => $settings->first()
       ));
    }
 
    public function store(Request $request)
    {         
       $data = Input::all();   
       try { 
           $settings = Settings::create($data);
           $request->session()->flash('alert', array('code'=> 'success', 'text'  => 'Operação realizada com sucesso!'));
           return redirect(route('configuracoes'));
       } catch (Exception $e) {
           $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
           return redirect(route('configuracoes'));
       }   
   }

   public function show($id)
   {
       $settings = Settings::find($id);        
       return view("admin.personagens.show", array(
           "character" => $settings
       ));
   }

   public function update(Request $request, $id)
   {  
      $data = Input::all();  
       try {           
           Settings::find($id)->update($data);
           $request->session()->flash('alert', array('code'=> 'success', 'text'  => 'Operação realizada com sucesso!'));
       } catch (Exception $e) {
           $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
       }
       return redirect(route('configuracoes'));
   }

}
