<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Character;

class PersonagensController extends Controller
{
 
    public function index(Request $request)
     {
         $Character = Character::orderBy("character_id", "DESC");    
         return view('admin.personagens.index', array(
            "personagens" => $Character->paginate(50)
        ));
     }
 
     public function create()
     {        
         return view("admin.personagens.show");
     }
 

     public function store(Request $request)
     {         
        $data = Input::all(); 
        if($request->hasFile('img')) {
            $file = $request->file('img');
            $input['imagename'] = md5(time()).'.'.$file->getClientOriginalExtension();
            $name_img = md5(time()).'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('img/');
            $file->move($destinationPath, $input['imagename']);
           $data['img'] = $name_img;
        }
        try { 
            $Character = Character::create($data);
            $request->session()->flash('alert', array('code'=> 'success', 'text'  => '<b>Personagem</b> criado com sucesso !!'));
            return redirect(route('personagens'));
        } catch (Exception $e) {
            $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
            return redirect(route('personagens'));
        }   
    }

    public function show($id)
    {
        $Character = Character::find($id);         
        return view("admin.personagens.show", array(
            "character" => $Character
        ));
    }

    public function update(Request $request, $id)
    {  
        $data = Input::all();  
        if($request->hasFile('img')) {

            $file = $request->file('img');
            $input['imagename'] = md5(time()).'.'.$file->getClientOriginalExtension();
            $name_img = md5(time()).'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('img/');
            $file->move($destinationPath, $input['imagename']);
        $data['img'] = $name_img;
        }
        try {           
            Character::find($id)->update($data);
            $request->session()->flash('alert', array('code'=> 'success', 'text'  => '<b>Personagem</b> editado com sucesso !'));
        } catch (Exception $e) {
            $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
        }
        return redirect(route('personagens'));
    }

    public function destroy(Request $request, $id)
    {
        try {
            $Character = Character::find($id);
            if(empty($Character)) {
                $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
                return redirect(route('personagens'));
            }
            $Character->delete();
            $request->session()->flash('alert', array('code'=> 'success', 'text'  => '<b>Personagem</b> deletado com sucesso !'));
        } catch (Exception $e) {
            $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
        }
        return redirect(route('personagens'));
    }

}
