<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $table       = "character";
    protected $primaryKey  = 'character_id';
    protected $fillable  = ['description', 'img', 'created_at', 'updated_at'];
}
