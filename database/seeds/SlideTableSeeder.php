<?php

use Illuminate\Database\Seeder;
use App\Models\Slide;

class SlideTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        slide::create([
            'img_background' => 'no-foto.png', 
            'img_principal' => 'no-foto.png'
        ]);
    }
}
