<?php

use Illuminate\Database\Seeder;
use App\Models\Settings;


class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        settings::create([
            'name' => 'TITULO SITE JOGO', 
            'presentation' => 'texto de apresentação',
            'description' => 'texto de descrição'
        ]);
    }
}
