<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       user::create([
           'name' => 'Dev Teste', 
           'email' => 'exemple@dev.com.br',
           'password' => bcrypt('123123'),
       ]);
    }
}
